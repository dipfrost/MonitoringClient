import requests
import json

#import decimal


def query(task,ip,port):
	#ip = "192.168.1.2"
	#port = "30889"
	m = "1"
	url = str("http://qwe111:ewq111@" + ip + ":" + port + "/") # 
	headers = {'content-type': 'application/json'}
	jgetinfo = {
		'jsonrpc': '1.0',
		'id': 'curltest',
	    "method": "getinfo"
	}

	joutputs = {
		'jsonrpc': '1.0',
		'id': 'curltest',
	    "method": "masternode",
	    "params": ['outputs']
	}
	jgetbalance = {
		'jsonrpc': '1.0',
		'id': 'curltest',
		"method": "getbalance"
	}
	jlistaddressgroupings = {
		'jsonrpc': '2.0',
		'id': 'curltest',
	    "method": "listaddressgroupings",
	#    "params": ['info'],
		#"filter": ['addr']
	}
	jlisttransactions = {
		'jsonrpc': '2.0',
		'id': 'curltest',
	    "method": "listtransactions",
	}

	jgetnewaddress = {
		'jsonrpc': '2.0',
		'id': 'curltest',
	    "method": "getnewaddress",
	    #"params": [''+params+''],
		#"filter": ['addr']
	}
	jgetaddressesbyaccount = {
		'jsonrpc': '2.0',
		'id': 'curltest',
	    "method": "getaddressesbyaccount",
	   # "params": [''+params+''],
		#"filter": ['addr']
	}
	jgetblock = {
		'jsonrpc': '2.0',
		'id': 'curltest',
	    "method": "getblock",
	#    "params": [''+params+'']
	}

	if task == 'getinfo':
		met = jgetinfo
	elif task == 'outputs':
		met = joutputs
	elif task == 'getbalance':
		met = jgetbalance
	elif task == 'listaddressgroupings':
		met = jlistaddressgroupings
	elif task == 'listtransactions':
		met = jlisttransactions
	elif task == 'getnewaddress':
		met = jgetnewaddress
	elif task == 'getaddressesbyaccount':
		met = jgetaddressesbyaccount
	elif task == 'getblock':
		met = jgetblock

	try:
		response = requests.post(
    		url, data=json.dumps(met, sort_keys=True, indent=4,ensure_ascii=False), headers=headers).json()
	except  Exception:
		return "Conection error"
	else:
		return response
	finally:
		pass
