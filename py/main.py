
import db as db
import socket
import os
import sys
import requests
import json
import subprocess
import re
import bs4
import time

from datetime import datetime


date=datetime.strftime(datetime.now(), "%H:%M:%S %Y-%m-%d")
f = open('/hostname')
loc=f.readlines()[0].rstrip()
#loc= "rivas-X299-UD4"


def addcontainer():
    
    node=sys.argv[2]
    listnods=db.mysql_conn("SELECT * FROM wallets WHERE name = '"+node+"' AND locate = '"+loc+"' ",0)

    for i in listnods:
        nodename=i[1].lower()
        daemon=i[13]
        cli=i[10]
        conffile=i[12]
        image=i[17]
        confdir=i[14]
        sentinel=listnods[0][15]
        if nodename != "" and daemon != "" and cli != "" and conffile != "" and image != "" and confdir != "" :
            os.system ("bash -c 'py/sh/addcontainer.sh %s %s %s %s %s %s %s'" % (nodename, daemon, cli, conffile, image, confdir, sentinel))
        else:
            print("Config not full")
	

def delcontainer():

    dockername=sys.argv[2]
    listnods=db.mysql_conn("SELECT nodename,dockernumber FROM containers WHERE locate = '"+loc+"' AND dockername='"+dockername+"' ",0)[0]
    nodename=listnods[0].lower()
    nodenumber=listnods[1]
    db.mysql_conn("DELETE FROM containers WHERE dockername ='"+dockername+"' ",1)
    os.system ("bash -c 'py/sh/delcontainer.sh %s %s %s'" % (nodename, nodenumber, dockername))


def updatemanycontainers():

    listnods=db.mysql_conn("SELECT * from wallets WHERE locate='"+loc+"' AND active='1'",0)
    for i in listnods:
        print(i[1])
        updatesinglecontainer(str(i[1]).lower())


def updatesinglecontainer(node):
    
    masternodestatustemp = """{
        "vin" : "CTxIn(COutPoint(0000000000, 4294967295), coinbase )",    
        "status" : "could not get",
        "pubKeyMasternode" : "VSSL8PQQBcnaMLPdhDVFwq6oEoSxN4pXeM"
    } """
    getinfotemp = """{
        "version" : "could not get",
        "blocks" : "could not get"
    } """

    reg = re.compile('[^0-9A-Za-z ]')
    reg1 = re.compile('[^0-9 ]')

    listnods=db.mysql_conn("SELECT name,cli from wallets WHERE locate='"+loc+"' AND name='"+node+"' ",0)
    if len(listnods) < 1 :
        print("node not exist")
    nodname=listnods[0][0].lower()
    cli=listnods[0][1]
    r=subprocess.check_output("docker ps -f name="+nodname+" -aq", shell=True);
    k=r.split("\n")
    for i in range(0,len(k)-1):
        m="2"

        try:
            masternodestatus=subprocess.check_output("docker exec -i "+k[i]+" bash -c ' '"+cli+"' masternode status'", shell=True,stderr=subprocess.STDOUT);
            parsed_masternodestatus = json.loads(masternodestatus)
            status=parsed_masternodestatus['status']
        except subprocess.CalledProcessError as e:
            try:
                masternodestatus=subprocess.check_output("docker exec -i "+k[i]+" bash -c ' '"+cli+"' smartnode status'", shell=True,stderr=subprocess.STDOUT);
                parsed_masternodestatus = json.loads(masternodestatus)
                status=parsed_masternodestatus['status']
            except subprocess.CalledProcessError as e:
                parsed_masternodestatus = json.loads(masternodestatustemp)
                status=parsed_masternodestatus['status']

        try:
            vin=reg.sub('', parsed_masternodestatus['vin'])
        except KeyError as e:
            try:
                vin=reg.sub('', parsed_masternodestatus['outpoint'])
            except KeyError as e:  
                try:
                    vin=reg.sub('', parsed_masternodestatus['txhash'])
                except KeyError as e:
                    vin=reg.sub('', parsed_masternodestatus['vin'])
#getinfo
        try:
            getinfo=subprocess.check_output("docker exec -i "+k[i]+" bash -c ' '"+cli+"' getinfo'", shell=True);
            parsed_getinfo = json.loads(getinfo)
            version=parsed_getinfo['version']
            blocks=parsed_getinfo['blocks']
        except subprocess.CalledProcessError as e:
            try:
                getinfo=subprocess.check_output("docker exec -i "+k[i]+" bash -c ' '"+cli+"' -getinfo'", shell=True);
                parsed_getinfo = json.loads(getinfo)
                version=parsed_getinfo['version']
                blocks=parsed_getinfo['blocks']
            except subprocess.CalledProcessError as e:
                parsed_getinfo = json.loads(getinfotemp)
                version=parsed_getinfo['version']
                blocks=parsed_getinfo['blocks']
        
        namecont=subprocess.check_output("docker ps -f id="+k[i]+" --format {{.Names}}", shell=True);
        dockercount=reg1.sub('', namecont)
        if "0000" in vin:
            m="4"    
        data = {'nodename': str(nodname),
                'dockername': str(k[i]),
                'blocks': str(blocks),
                'status': str(status),
                'version': str(version),
                'vin': str(vin),
                'active_status': str(m),
                'timestamp': str(date),                                        
                'locate': str(loc),
                'dockernumber': str(dockercount),
                'genkey': str(getgenkey(k[i],nodname,dockercount))
                }

        req = requests.post('http://192.168.1.44:8009/api/containers', data=json.dumps(data))
        updatecontainerstatus()


def getgenkey(dockername,nodename,dockernumber):
    
    dbwallets=db.mysql_conn("SELECT conffile,confdir FROM wallets WHERE name = '"+nodename+"' AND locate = '"+loc+"' ",0)
    conffile = dbwallets[0][0]
    confdir = dbwallets[0][1]
    res=subprocess.check_output("echo $(cat /dockerconfig/%s%s/%s | grep masternodeprivkey | sed 's/masternodeprivkey=//g')" % (nodename, dockernumber, conffile), shell=True,stderr=subprocess.STDOUT);
    return res.rstrip()


def updatecontainerstatus():

    listnotactive=db.mysql_conn("SELECT * from containers WHERE active_status='4' ",0)
    for i in range(len(listnotactive)):
        db.mysql_conn("UPDATE wallets set work='1' WHERE name='"+listnotactive[i][1]+"' AND locate='"+listnotactive[i][10]+"' ",1)
        
    listactive=db.mysql_conn("SELECT * from wallets WHERE work='1' ",0)
    for j in range(len(listactive)):
        listactive1=db.mysql_conn("SELECT COUNT(*) from containers WHERE nodename='"+listactive[j][1]+"' AND active_status='4' ",0)
        if [listactive1][0][0][0] < 1:
            db.mysql_conn("UPDATE wallets set work='0' WHERE name='"+listactive[j][1]+"' AND locate='"+listactive[j][9]+"' ",1)
    
    
def reindex(dockername):

    nodename=db.mysql_conn("SELECT nodename FROM containers WHERE dockername = '"+dockername+"' ",0)[0][0]
    print(nodename)
    daemon=db.mysql_conn("SELECT daemon FROM wallets WHERE name = '"+nodename+"' AND locate='"+loc+"' ",0)[0][0]
    cli=db.mysql_conn("SELECT cli FROM wallets WHERE name = '"+nodename+"' AND locate='"+loc+"' ",0)[0][0]
    os.system ("bash -c 'py/sh/reindex.sh %s %s %s'" % (dockername, cli, daemon))



def getdockerimages():

    os.system ("bash -c 'py/sh/getdockerimages.sh'")    
    res=db.mysql_conn("SELECT id FROM clients WHERE name='"+loc+"'",0)
    userid=res[0][0]
    db.mysql_conn("DELETE FROM docker WHERE locate='"+loc+"'",1)
    f = open('images.lg')
    for line in f:
        line=line.split(":")
        repository=line[0].rstrip()
        tag=line[1].rstrip()
        imageid=line[2].rstrip()
        db.mysql_conn("INSERT INTO docker (image,tag,locate,imageid,userid) VALUES ('"+str(repository)+"','"+str(tag)+"','"+str(loc)+"','"+str(imageid)+"','"+str(userid)+"')",1)


def updatewallet():

    import rpc
    nods=db.mysql_conn("SELECT * FROM wallets WHERE locate='"+loc+"' AND active='1' ",0)
    for i in range(len(nods)):
        count= db.mysql_conn("SELECT COUNT(*) FROM `containers` WHERE `containers`.`nodename` = '"+nods[i][1]+"' AND locate = '"+loc+"'  ",0)[0][0]    
        db.mysql_conn("UPDATE wallets set status="+str(count)+" WHERE name='"+nods[i][1]+"' AND locate='"+loc+"' ",1)
        if nods[i][2] != None and nods[i][3] != None :            
            if rpc.query("getinfo",nods[i][2],nods[i][3]) == "Conection error" :
                db.mysql_conn("UPDATE wallets set outputs='ConectionError' where name='"+nods[i][1]+"' AND locate='"+loc+"' ",1)
            else:
                a=rpc.query("getinfo",nods[i][2],nods[i][3])["result"]["version"]
                a=str(a).replace(".","").replace("-","").replace("v","")
                b=rpc.query("getinfo",nods[i][2],nods[i][3])["result"]["blocks"]
                c=len(rpc.query("outputs",nods[i][2],nods[i][3])["result"])
                a=str(a).replace(".","").replace("-","").replace("v","")
                db.mysql_conn("UPDATE wallets set blocks="+str(b)+",version="+str(a)+",outputs="+str(c)+" WHERE name='"+nods[i][1]+"' AND locate='"+loc+"' ",1)
        else:
            db.mysql_conn("UPDATE wallets set outputs='SettingsError' where name='"+nods[i][1]+"' AND locate='"+loc+"' ",1)
        db.mysql_conn("UPDATE wallets set timestamp='"+date+"' WHERE name='"+nods[i][1]+"' AND locate='"+loc+"' ",1)          
    

def getcoininfo():
    listnods=mysql_conn("SELECT name from wallets WHERE locate='"+loc+"' ",0)
    for i in listnods:
       print(i[0].lower())
    

def getallnods():
    os.system ("bash -c 'py/sh/getallcontainers.sh'")   
    f = open('sort.txt')
    for line in f:
        name=line.rstrip()

        if db.mysql_conn("SELECT COUNT(*) FROM wallets WHERE name='"+name+"' AND locate='"+loc+"'",0)[0][0] == 0 :
            clients=db.mysql_conn("SELECT ip FROM clients WHERE name='"+loc+"'",0)
            ipaddr=clients[0][0]
            db.mysql_conn("INSERT INTO wallets (name,locate,ip) VALUES ('"+str(name)+"','"+str(loc)+"','"+str(ipaddr)+"')",1)    
  
  
def getconfigs():
    listnods=db.mysql_conn("SELECT name from wallets WHERE locate='"+loc+"' AND active='1' ",0)
    for i in listnods:
        os.system ("bash -c 'py/sh/getconfigs.sh %s'" % (i[0].lower()))
        os.system ("bash -c 'py/sh/getdaemons.sh %s'" % (i[0].lower()))
        with open('getdaemons.lg') as f:
            rowcounts=(sum(1 for _ in f))
            if rowcounts > 1:
                fd = open('getdaemons.lg')
                cli=fd.readline().rstrip()
                daemon=fd.readline().rstrip()
            else:
                fd = open('getdaemons.lg')
                resstring=fd.readlines()[0].rstrip()
                daemon=resstring
                cli=resstring
        f = open('getconfigs.lg')
        resstring=f.readlines()[0].rstrip().split("/")
        dockedir=resstring[0]
        dockfile=resstring[1]
        db.mysql_conn("UPDATE wallets set daemon='"+str(daemon)+"', cli='"+str(cli)+"', confdir='"+str(dockedir)+"', conffile='"+str(dockfile)+"' WHERE name='"+i[0]+"' ",1)


def restartcontainer():
    if len(sys.argv) > 2:
        dockerid=sys.argv[2]
        subprocess.check_output("docker restart "+dockerid+"", shell=True,stderr=subprocess.STDOUT);

def getPrice():
    parseFromMNO()
    listalias=db.mysql_conn("SELECT alias from wallets WHERE alias != 'NULL'",0)
    for i in listalias:
        price=db.mysql_conn("SELECT price,changev from coins WHERE alias ='"+i[0]+"' ",0)
        if len(price) > 0 :
            db.mysql_conn("UPDATE wallets set price='"+price[0][0]+"' WHERE alias='"+i[0]+"' ",1)
            db.mysql_conn("UPDATE wallets set changev='"+price[0][1]+"' WHERE alias='"+i[0]+"' ",1)
            if float(price[0][1]) > 0.0:
                db.mysql_conn("UPDATE wallets set profit='good' WHERE alias='"+i[0]+"' ",1)            
            if float(price[0][1]) < 0.0:
                db.mysql_conn("UPDATE wallets set profit='bad' WHERE alias='"+i[0]+"' ",1)
            if float(price[0][1]) == 0.0:
                db.mysql_conn("UPDATE wallets set profit='greey' WHERE alias='"+i[0]+"' ",1)


def parseFromMNO():
    headers = {'path': "/?convert=BTC",
                'referer': "https://masternodes.online/",
                'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
                'authority': "masternodes.online"
            }

    response = requests.get('https://masternodes.online/?convert=BTC', headers=headers) 
    b=bs4.BeautifulSoup(response.text, "html.parser")
    p3=b.select('.table tr')
    for i in range(1,len(p3)-1):
        st = p3[i].getText().strip().split("\n")
        if len(st[0].split("(")) > 1:
            alias = st[0].split("(")[1][:-1]
            price = st[1]
            change = st[2][:-2]
            if db.mysql_conn("SELECT COUNT(*) from coins WHERE alias = '"+alias+"' ",0)[0][0] > 0 :
                db.mysql_conn("UPDATE coins set price='"+price+"',alias='"+alias+"',changev='"+change+"' WHERE alias='"+alias+"' ",1)
            else:
                db.mysql_conn("INSERT INTO coins (price,alias,changev) VALUES ('"+str(price)+"','"+str(alias)+"','"+str(change)+"')",1)

def updateall():
    getconfigs()
    updatewallet() 
    updatemanycontainers()
    updatecontainerstatus()

def choice():
    if len(sys.argv) > 1:
        if sys.argv[1] == "addcontainer":
            addcontainer()
        elif sys.argv[1] == "delcontainer":
            delcontainer()
        elif sys.argv[1] == "updatemanycontainers":
            updatemanycontainers()
        elif sys.argv[1] == "updatesinglecontainer":
            updatesinglecontainer(sys.argv[2])
        elif sys.argv[1] == "updatecontainerstatus":
            updatecontainerstatus()        
        elif sys.argv[1] == "reindex":
            reindex(sys.argv[2])
        elif sys.argv[1] == "getdockerimages":
            getdockerimages()
        elif sys.argv[1] == "updatewallet":
            updatewallet()    
        elif sys.argv[1] == "getcoininfo":
            getcoininfo()     
        elif sys.argv[1] == "getprice":
            getPrice()                 
        elif sys.argv[1] == "parsefrommno":
            parseFromMNO()                   
        elif sys.argv[1] == "getconfigs":
            getconfigs()                 
        elif sys.argv[1] == "getallnods":
            getallnods()     
        elif sys.argv[1] == "delcontainer":
            delcontainer()       
        elif sys.argv[1] == "restartcontainer":
            restartcontainer()                         
        elif sys.argv[1] == "updateall":
            getconfigs()
            updatewallet() 
            updatemanycontainers()
            updatecontainerstatus()
        else : 
            print("else")

choice()        

