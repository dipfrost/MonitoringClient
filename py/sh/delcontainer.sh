#!/bin/bash

nodename=$1
nodenumber=$2
dockerid=$3


dir="/dockerconfig/"

echo "REMOVE DOCKER CONTAINER ${dockerid}" 
doc=$(docker rm -f ${dockerid})
echo "REMOVE DIR ${dir}${nodename}${nodenumber}" 
rm -r ${dir}${nodename}${nodenumber}

echo "Done"
