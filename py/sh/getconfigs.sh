
rm getconfigs.lg
nodename=$1
nodeid=$(docker ps -f name=$nodename'1'$ -aq)
echo $(docker exec -i $nodeid bash -c "find ~ -maxdepth 2 -type f  -iname '*.conf'  -not -name 'masternode.conf' -not -name 'ccache.conf' | sed -r 's/.root.//g'") > getconfigs.lg