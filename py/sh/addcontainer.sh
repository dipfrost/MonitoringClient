#!/bin/bash

#nodename, daemon, cli, conffile, image

nodename=$1
daemon=$2
cli=$3
conffile=$4
image=$5
confdir=$6
sentinel=$7

listdir="/dockerconfig/"
dockerdir=$(cat /dockerdir)

if [[ $sentinel = '1' ]]; then
        sentinel="service cron start;"
else
        sentinel=""
fi

function getnumbernewnode {
        echo "<br>"
        echo "==================Get node number======================================="
        echo "<br>"
        echo ${listdir}${nodename}1

        if [ -d ${listdir}${nodename}1 ]; then
                nodecount=1
                while [ -d ${listdir}${nodename}${nodecount} ]
                do
                        (( nodecount++ ))
                done
                echo "<br>"
                echo 'Node number: ' $nodecount
        fi
}


function copyblockfolder {
        echo "<br>"
        echo "==================Copy blocks==========================================="
        echo "<br>"
        if ! [ -d ${listdir}${nodename}${nodecount} ]; then
                echo "cp -r ${listdir}${nodename}1 ${listdir}${nodename}${nodecount}"
                cp -r ${listdir}${nodename}1 ${listdir}${nodename}${nodecount}
        else
                echo "Already exist!!!!!"
                exit 0
        fi
        echo "<br>"
        echo "==================END=================================================="
}


function detectnode {
        echo "<br>"
        echo "==================CREATE DOCKER CONTAINER==============================="
        echo "<br>"
        echo "${nodename}, keep going..."
        echo "<br>"
        nodid=$(docker run -v ${dockerdir}${nodename}${2}:/root/${confdir} --name ${nodename}${2} -d ${image} /bin/sh -c "${sentinel} ${daemon} -daemon; while true; do ping 8.8.8.8; done")
        echo "Container ID = ${nodid}"
        echo "<br>"
        echo "==================END CREATE DOCKER CONTAINER==========================="
}


function genkey {
        echo "<br>"
        echo "==================Wait 30s to responce=================================="
        echo "<br>"
        lenex=45
        sleep 10s

        while true;
        do

                genkey=$(docker exec -i ${nodid} /bin/bash -c "${cli} masternode genkey")
                len=$(echo $genkey | wc -c)
                if [[ "$len" > "$lenex" ]]; then
                        break
                else
                echo "loop waiting genkey"
                echo "<br>"
                sleep 30s

                fi
        done

        echo "$nodename GENKEY = ${genkey}"
        echo "<br>"
}


function changegenkey {
        echo "<br>"
        echo "cd ${listdir}${nodename}${nodecount}"
        echo "<br>"
        cd ${listdir}${nodename}${nodecount}
        sed 's/^masternodeprivkey=.*/masternodeprivkey='${genkey}'/g' ${conffile} | sponge ${conffile}
        echo "==================Restart cointainer===================================="
        echo "<br>"
        docker restart ${nodid}
        echo "<br>"
        echo "======================================DONE=============================="
        echo "<br>"
        echo "$nodename GENKEY = ${genkey}"
        echo "<br>"
        echo "======================================DONE=============================="
        echo "<br>"

}



getnumbernewnode
copyblockfolder
detectnode $nodename $nodecount
genkey $nodename
changegenkey $nodename

