rm getdaemons.lg
nodename=$1
nodeid=$(docker ps -f name=$nodename'1'$ -aq)

echo $(docker exec -i $nodeid bash -c "find /usr/local/bin -maxdepth 1 -type f -iname '*cli'  | sed -r 's/.usr.local.bin.//g'") > getdaemons.lg

co=$(cat getdaemons.lg | wc -c)
if [[ $co -ge 2 ]]; then
        echo $(docker exec -i $nodeid bash -c "find /usr/local/bin -maxdepth 1 -type f -iname '*d'  | sed -r 's/.usr.local.bin.//g'") >> getdaemons.lg
else
        echo $(docker exec -i $nodeid bash -c "find /usr/local/bin -maxdepth 1 -type f -iname '*d'  | sed -r 's/.usr.local.bin.//g'") > getdaemons.lg
fi