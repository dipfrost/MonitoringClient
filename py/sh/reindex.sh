
#1arg dockname 2arg cli 3arg daemon
#echo "$1 stop; find . -not -name "wallet.dat" -not -name "*.conf" -print0  | xargs -0 rm -r; nohup innovad -reindex -daemon "
docker exec -it $1 /bin/bash -c "$2 stop; find . -not -name "wallet.dat" -not -name "*.conf" -print0  | xargs -0 rm -r; nohup $3 -reindex -daemon"
#docker exec -it $1 /bin/bash -c "$2 stop; nohup $3 -reindex -daemon"