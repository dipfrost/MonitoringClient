package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
)

var port = "8181"
var compname = ""
var compip = ""

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func handler(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	name := r.URL.Query()["name"]
	loc := r.URL.Query()["loc"]
	if len(loc) != 0 {
		fmt.Fprintf(w, "<a href='http://192.168.1.44:8080/containers.php?id=%s&loc=%s'><h1>Back</h1></a><br><br>", name[0], loc[0])
	} else {
		fmt.Fprintf(w, "<a href='http://192.168.1.44:8080/index.php'>MAIN</a><br><br>")
	}

	script := r.URL.Query()["script"]
	if len(name) == 0 || len(script) == 0 {
		log.Println("EMPTY VALUES")
	} else {
		out, err := exec.Command("/usr/bin/python", "py/main.py", script[0], name[0]).Output()
		log.Println("EXEC " + "scpipt: " + script[0] + " name: " + name[0] + "\n")
		if err != nil {
			fmt.Printf("ERROR: %s", err)
			fmt.Printf("RESPONCE: %s", out)
		}
		fmt.Fprintf(w, "%s", out)
	}

}

func getname() string {
	file, err := os.Open("/hostname")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		compname = scanner.Text()
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return string(compname)
}

func getip() string {
	file, err := os.Open("/ip")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		compip = scanner.Text()
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return string(compip)
}

func invite() {

	data := []byte(`{"name":"` + getname() + `","ip":"` + getip() + `","port":"` + port + `"}`)
	r := bytes.NewReader(data)
	resp, err := http.Post("http://192.168.1.44:8009/api/clients", "application/json", r)
	fmt.Println(err)
	fmt.Println(resp)

}

func main() {
	log.Println("CLIENT START")
	invite()
	http.HandleFunc("/", handler)
	http.ListenAndServe("0.0.0.0:"+port, nil)
}
