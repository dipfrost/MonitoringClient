FROM golang


COPY ./client.go /go

RUN cd /go \


  && apt-get update \
  && apt-get install python-setuptools -y \
  && apt-get install moreutils -y \
  && easy_install pip \
  && pip install mysql-connector \
  && pip install beautifulsoup4 \
  && pip install requests \
  && go get github.com/go-sql-driver/mysql \
  && go get github.com/gin-gonic/gin \
  && go build ./client.go


ADD ./py /go
ADD ./scripts /go

ENTRYPOINT ["./start-unprivileged.sh"]